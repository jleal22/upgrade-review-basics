// Iteración #6: Función swap

// Crea una función llamada `swap()` que reciba un array y dos parametros que sean indices del array. La función deberá intercambiar la posición de los valores de los indices que hayamos enviado como parametro. Retorna el array resultante.

// Sugerencia de array:

players = ['Mesirve', 'Cristiano Romualdo', 'Fernando Muralla', 'Ronalguiño']

function swap(param, pos1, pos2) {
    let temp = param[pos1];
    param[pos1] = param[pos2];
    param[pos2] = temp;
    return param;
}

console.log(swap(players, 1, 2));
console.log(swap(players, 0, 3));
console.log(swap(players, 2, 1));