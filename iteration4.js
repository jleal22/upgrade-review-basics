// Iteración #4: Métodos findArrayIndex

// Crea una función llamada `findArrayIndex` que reciba como parametros un array de textos y un texto y devuelve la posición del array cuando el valor del array sea igual al valor del texto que enviaste como parametro. Haz varios ejemplos y compruebalos.

const example1 = ['Caracol', 'Mosquito', 'Salamandra', 'Ajolote'];
const example2 = ['uno', 'dos', 'tres', 'cuatro', 'cinco'];

function findArrayIndex(array, text) {
    for (let i = 0; i < array.length; i++) {
        if (text == array[i]) return i;
    }
    return -1;
}

console.log(findArrayIndex(example1, 'Mosquito'));
console.log(findArrayIndex(example1, 'Ballena'));
console.log(findArrayIndex(example2, 'tres'));
console.log(findArrayIndex(example2, 'cinco'));
